# JPA One to Many Relationship Example with Spring Boot, Maven, and MySQL

## Guide


## Prerequisites
- JDK 1.8 or later
- Maven 3 or later
- MySQL 5.6 or later

## Stack
- Spring Data JPA
- Spring Boot
- MySQL

## Run
`mvn spring-boot:run`

